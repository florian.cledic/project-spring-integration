# Projet Spring Integration : Order Management

**Author** : ABELHAJ Youssef, CLEDIC Florian

**School** : EFREI Paris

**Promotion** : 2022

**Class** : M2-APP-LS1 - DT (Digital Transformation)

**Language used** : Java 11

**Framework used** : Spring Boot, Spring Integration

**Additional information** : This project was made 
using pair-programming method, (we both worked on the project, 
but only commits of the person who made the changes are visible).

## Description

> This programme make a treatment in a file to find `ramen` 
> items and calculate all cost of the order.

This Program :

1. Take a file contain order without total price
```json
{
  "commandId": 1,
  "date": "2021-12-15T17-15-00Z",
  "orderItems": [
    {
      "count" : 1,
      "itemName" : "regular pork ramen",
      "type" : "ramen",
      "costAtUnit" : "7.50"
    },
    {
      "count" : 2,
      "itemName" : "black soy chicken ramen",
      "type" : "ramen",
      "costAtUnit" : "8.50"
    },
    {
      "count" : 1,
      "itemName" : "classic cheesecake",
      "type" : "dessert",
      "costAtUnit" : "1.50"
    }
  ]
}
```

2. Get all items in the order where item is `ramen` and write it in another file in out path
```json
{
  "count": 1,
  "itemName": "regular pork ramen",
  "type": "ramen",
  "costAtUnit": 7.5,
  "costAtTotal": 0.0
}
{
  "count": 2,
  "itemName": "black soy chicken ramen",
  "type": "ramen",
  "costAtUnit": 8.5,
  "costAtTotal": 0.0
}
```
3. Calculate total cost with all items described in the file 
and write it in another file in out path
```json
{
  "commandId": 1,
  "date": "2021-12-15T17-15-00Z",
  "orderItems": [
    {
      "count": 1,
      "itemName": "regular pork ramen",
      "type": "ramen",
      "costAtUnit": 7.5,
      "costAtTotal": 7.5
    },
    {
      "count": 2,
      "itemName": "black soy chicken ramen",
      "type": "ramen",
      "costAtUnit": 8.5,
      "costAtTotal": 17.0
    },
    {
      "count": 1,
      "itemName": "classic cheesecake",
      "type": "dessert",
      "costAtUnit": 1.5,
      "costAtTotal": 1.5
    }
  ],
  "totalCost": 26.0
}
```