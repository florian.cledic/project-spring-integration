package net.efrei.project_spring_integration.model;

import lombok.Data;

import java.io.Serializable;

@Data
public class OrderItem implements Serializable {

    private int count;
    private String itemName;
    private TypeItem type;
    private float costAtUnit;
    private float costAtTotal;

}
