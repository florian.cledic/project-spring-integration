package net.efrei.project_spring_integration.service;

import lombok.extern.log4j.Log4j2;
import net.efrei.project_spring_integration.model.Order;
import net.efrei.project_spring_integration.model.OrderItem;
import net.efrei.project_spring_integration.model.TypeItem;
import org.springframework.integration.annotation.Filter;
import org.springframework.stereotype.Service;

@Service
@Log4j2
public class GlobalService {

    public Order calculateTotalCost(Order order){
        log.info("[calculateTotalCost] [+]");
        order.getOrderItems().forEach(orderItem -> {
            orderItem.setCostAtTotal(orderItem.getCostAtUnit() * orderItem.getCount());
            if(order.getTotalCost() == 0)
                order.setTotalCost(orderItem.getCostAtTotal());
            else
                order.setTotalCost(order.getTotalCost() + orderItem.getCostAtTotal());
        });
        log.info("[calculateTotalCost] order total cost : " + order.getTotalCost());
        log.info("[calculateTotalCost] [-]");
        return order;
    }

    @Filter
    public boolean itemTypeIsRamen(OrderItem item) {
        log.info("[itemTypeIsRamen] item test = " + item);
        return item.getType().equals(TypeItem.ramen);
    }

}
