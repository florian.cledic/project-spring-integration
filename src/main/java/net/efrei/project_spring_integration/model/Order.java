package net.efrei.project_spring_integration.model;

import lombok.Data;

import java.io.Serializable;
import java.util.List;

@Data
public class Order implements Serializable {

    private int commandId;

    private String date;

    private List<OrderItem> orderItems;

    private float totalCost;

}
